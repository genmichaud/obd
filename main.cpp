#include <stdio.h>   /* Standard input/output definitions */
#include <cstdlib>
#include <iostream>
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h> /* exit */
#include <unistd.h> /* read, write, close */
#include <sys/socket.h> /* socket, connect */
#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <netdb.h> /* struct hostent, gethostbyname */
#include <curl/curl.h>
#include <errno.h>
#include <string.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <signal.h>

#include <OBD2UART.h>

using namespace std;


enum sensorId { TEMPERATURE = 0, HUMIDITY = 1, RPM = 2};
const char *sensorNames[] = {"tem", "hud", "rpm"};
struct SensorData {
    sensorId id;
    float value;
};

FILE *csv;

int open_port(void)
{
  int fd; /* File descriptor for the port */


  fd = open("/dev/rfcomm99", O_RDWR | O_NOCTTY | O_NDELAY); 
  if (fd == -1)
  {
   /*
    * Could not open the port.
    */

    perror("open_port: Unable to open /dev/rfcomm99 - ");
  }
  else
    fcntl(fd, F_SETFL, 0);

  return (fd);
}


void error(const char *msg) { perror(msg); exit(0); }

size_t write_data(void *buffer, size_t size, size_t nmemb, void *userp)
{
   return size * nmemb;
}

void sendData(SensorData& data){
    CURL *curl;
    CURLcode res;
    struct curl_slist *headers = NULL;
    curl_global_init(CURL_GLOBAL_DEFAULT);
    
    headers = curl_slist_append(headers, "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiR1JPVVBfMjAifQ.LOa2fLbZYRTgD6QRYDLZgHQIBCV97a5wQfE-SMOfFJU");
    headers = curl_slist_append(headers, "Content-Type: application/json");

    curl = curl_easy_init();
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_URL, "https://iot-project-metropolia.eu-gb.mybluemix.net/api/group_20");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
        char buffer[100];
        sprintf(buffer, "{ \"value\": %f, \"sensor_name\": \"%s\" }", data.value, sensorNames[data.id]);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, buffer);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);

        #ifdef SKIP_PEER_VERIFICATION
            curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        #endif

        #ifdef SKIP_HOSTNAME_VERIFICATION
            curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
        #endif
        /* Perform the request, res will get the return code */ 
        res = curl_easy_perform(curl);
        /* Check for errors */ 
        if(res != CURLE_OK)
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));
   
        /* always cleanup */ 
        curl_easy_cleanup(curl);
    }

    curl_global_cleanup();
}

void getSensors(float &temperature, float &humidity) {
    int file;
    char filename[40];
    const char *buffer;
    int addr = 0x27;        // The I2C address of the ADC

    sprintf(filename,"/dev/i2c-2");
    if ((file = open(filename,O_RDWR)) < 0) {
        printf("Failed to open the bus.");
        /* ERROR HANDLING; you can check errno to see what went wrong */
        exit(1);
    }

    if (ioctl(file,I2C_SLAVE,addr) < 0) {
        printf("Failed to acquire bus access and/or talk to slave.\n");
        /* ERROR HANDLING; you can check errno to see what went wrong */
        exit(1);
    }

    char buf[10] = {0};
    int tmp;

    // Using I2C Read
    if (read(file,buf,4) != 4) {
        /* ERROR HANDLING: i2c transaction failed */
        printf("Failed to read from the i2c bus.\n");
    } else {
	humidity = ((buf[0] & 0x3f) << 8) + buf[1];
	temperature = ((buf[2] << 8) + buf[3]) >> 2;
	temperature = temperature / ((1 << 14) - 2) * 165 - 40;
	humidity = humidity / ((1 << 14) - 2) * 100.;
    }

    buf[0] = 0b11110000;

    if (write(file,buf,1) != 1) {
        /* ERROR HANDLING: i2c transaction failed */
        printf("Failed to write to the i2c bus.\n");
    }
}

void intHandler(int dummy) {
    printf("\033[H\033[J");
    printf("Close signal read \n");
    if(csv){
	    cout << "Closing CSV file [ .. ]" << flush;
        fclose(csv);
	    cout << "\rClosing CSV file [ OK ]" << endl;
	}
	exit(0);
}

int main(int argc, char *argv[]){
	int fd = open_port();
	int flags = fcntl(fd, F_GETFL, 0);
	fcntl(fd, F_SETFL, flags | O_NONBLOCK);
	
	signal(SIGINT, intHandler);
	
    printf("\033[H\033[J");
	cout << "Creating OBD object [ .. ]" << flush;
	COBD obd(fd);
	cout << "\rCreating OBD object [ OK ]" << endl;
	cout << "Starting OBD [ .. ]" << flush;
	obd.begin();
	cout << "\rStarting OBD [ OK ]" << endl;
	cout << "Init OBD [ .. ]" << flush;
	while (!obd.init())printf("No init \n");
	cout << "\rInit OBD [ OK ]" << endl;
	cout << "Opening CSV file [ .. ]" << flush;
	csv = fopen("output.csv", "w+");
	cout << "\rOpening CSV file [ OK ]" << endl;
	usleep(500000);
    cout << "\033[H\033[J" << flush;
	
    SensorData temperature, humidity, rpm;
    temperature.id = TEMPERATURE;
    humidity.id = HUMIDITY;
    rpm.id = RPM;
    int rpm_value = 0;
    int speed_value = 0;
    while(1){
        getSensors(temperature.value, humidity.value);
		obd.readPID(PID_RPM, rpm_value);
		obd.readPID(PID_SPEED, speed_value);
		rpm.value = rpm_value;
        printf("Temperature readed : %f °C \n", temperature.value);
        printf("Humidity readed    : %f \% \n", humidity.value);
        printf("RPM readed         : %f \% \n", rpm.value);
        printf("Speed readed       : %d mph \n", speed_value);
        fprintf(csv, "1000,10c, %d \n", rpm_value);
        fprintf(csv, "1000,10d, %d \n", speed_value);
        sendData(humidity);
        usleep(500000);
        sendData(temperature);
        usleep(500000);
        sendData(rpm);
        printf("\r\b\r\b\r\b\r\b\r");
    }
	return EXIT_SUCCESS;
}
